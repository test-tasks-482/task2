fn distance_to(p1: &(f32, f32), p2: &(f32, f32)) -> f32 {
    ((p1.0 - p2.0).powf(2f32) + (p1.1 - p2.1).powf(2f32)).sqrt()
}

fn count_boomerangs(points: &[(f32, f32)]) -> usize {
    // If has no sense to continue
    if points.len() < 3 {
        return 0;
    }
    let mut result = 0;
    points.iter().for_each(|a| {
        // It was way much easier if we can use f32 as key in hashmap
        let mut distances = Vec::new();
        points.iter().for_each(|b| {
            if a == b {
                return;
            }
            distances.push(distance_to(a, b));
        });
        // Without hashmap it's have quiet big difficulty
        let mut processed = Vec::new();
        for base in distances.iter() {
            if processed.contains(base) {
                continue;
            }
            processed.push(*base);
            let count = distances.iter().filter(|d| *d == base).count();
            result += count * (count - 1);
        }
    });
    result
}

fn main() {
    let a = [(0f32, 0f32), (1f32, 0f32), (2f32, 0f32)];
    let r = count_boomerangs(&a);
    println!("Count: {}", r);
}

#[cfg(test)]
mod tests {
    use crate::count_boomerangs;

    #[test]
    fn test_count1() {
        let a = [(0f32, 0f32), (1f32, 0f32), (2f32, 0f32)];
        let r = count_boomerangs(&a);
        assert_eq!(r, 2);
    }

    #[test]
    fn test_count2() {
        let a = [(1f32, 1f32), (2f32, 2f32), (3f32, 3f32)];
        let r = count_boomerangs(&a);
        assert_eq!(r, 2);
    }

    #[test]
    fn test_count_bad() {
        let a = [(1f32, 1f32)];
        let r = count_boomerangs(&a);
        assert_eq!(r, 0);
    }
}